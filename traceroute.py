import asyncio
import os

MTR_BINARY = os.getenv('MTR_BINARY', 'mtr')

# thanks to https://fredrikaverpil.github.io/2017/06/20/async-and-await-with-subprocesses/

async def run_command(*args):
    """Runs a command that is given in the args list

    :param args: arguments including the command
    :return: a tuple containing the return code and output from stdout
    """
    try:
        process = await asyncio.create_subprocess_exec(
            *args,
            stdout=asyncio.subprocess.PIPE
        )
    except FileNotFoundError:
        return (127, "") # 127 return code means file not found

    stdout, _ = await process.communicate()

    result = stdout.decode().strip()

    return (process.returncode, result)

async def run_traceroute(host, port=None, protocol="tcp"):
    """Asynchronously runs a traceroute

    :param host: the host to traceroute
    :param port: which port to use
    :param protocol: which protocol to use for the traceroute
    :return: mtr traceroute output
    """
    print ("RUNNING TRACEROUTE")

    args = [MTR_BINARY, "--aslookup", "--report", "--json"]

    if port is not None:
        args.append(f"--port {port}")

    if protocol == "tcp":
        args.append("--tcp")
    elif protocol == "udp":
        args.append("--udp")

    args.append(host)
    print(' '.join(args))

    traceroute_output = await run_command(*args)
    print 
    return traceroute_output
