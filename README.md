# vtrace-probe

Works together with vtrace to provide traceroutes to the data collection service.

## Testing

You can run tests with the command:

```
pytest tests/
```
