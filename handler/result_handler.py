import asyncio
import logging

async def consume_result(result_queue, socket_handler):
    while True:
        item = await result_queue.get()
        await socket_handler.emit('result', item)