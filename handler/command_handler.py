import sys
import os
sys.path.append('..')

import time
import asyncio
import logging
import traceroute
from protocol.Error import Error

COMMAND_TYPES = ['traceroute']

log = logging.getLogger('vtrace-probe')
log.setLevel(logging.DEBUG)

async def traceroute_command(command):
    args = command["arguments"]
    if not 'host' in args:
        return {
            "success": False,
            "error": Error.TRACEROUTE_HOST_IS_UNSPECIFIED
        }
    
    host = args['host']
    port = None if not 'port' in args else args['port']
    protocol = "tcp" if not 'protocol' in args else args['protocol']

    mtr_output = await traceroute.run_traceroute(host, port, protocol)
    print("JOB DONE", command["id"], time.time())
    return mtr_output

def log_command_failure(uuid):
    log.debug(f'Received command was invalid, skipping over it (UUID: {uuid})')

def failed_command_error(uuid, error):
    return {
        'id': uuid,
        'result': {
            "success": False,
            'error': error
        }
    }

async def consume_command(queue, result_queue):
    while True:
        item = await queue.get()
        if not 'id' in item:
            log_command_failure(None)
            continue
        
        uuid = item['id']

        if not 'type' in item:
            log_command_failure(item['id'])
            await result_queue.put(failed_command_error(uuid, Error.COMMAND_TYPE_NOT_DEFINED))
            continue

        if not item['type'] in COMMAND_TYPES:
            log_command_failure(item['id'])
            await result_queue.put(failed_command_error(uuid, Error.COMMAND_TYPE_DOES_NOT_EXIST))
            continue

        if not 'arguments' in item:
            log_command_failure(item['id'])
            await result_queue.put(failed_command_error(uuid, Error.COMMAND_ARGUMENTS_NOT_DEFINED))
            continue
        
        log.debug(f'Received a command for consumption (UUID: {item["id"]})')

        result_item = {
            'id': item['id'],
            'result': {}
        }

        if item['type'] == 'traceroute':
            result = await traceroute_command(item)
            result_item['result'] = result
        
        await result_queue.put(result_item)