import sys, json
import asyncio
import socketio

class SocketHandler:
    def __init__(self, server, queue):
        self.state = {
            "connected": False,
            "registered": False
        }
    
        
        self.sio = socketio.AsyncClient()
        self.server = server
        self.command_queue = queue

        self.sio.on('connect', self.on_connect)
        self.sio.on('disconnect', self.on_disconnect)

        self.sio.on('message', self.on_message)
        self.sio.on('ping', self.on_ping)
        self.sio.on('command', self.on_command)

    def is_connected(self):
        return self.state['connected']
    
    def is_registered(self):
        return self.state['registered']

    async def connect(self):
        try:
            await self.sio.connect(self.server)
            self.state['connected'] = True
        except:
            # FIXME: log unsuccessful connection failure
            return
    
    async def emit(self, event, data):
        return await self.sio.emit(event, data)

    async def on_disconnect(self):
        self.state['connected'] = False
        print('disconnected from ws')

    async def on_connect(self):
        self.state['connected'] = True
        print('connected to ws')

    async def on_command(self, command):
        print('received command:', command)
        await self.command_queue.put(command)

    async def on_message(self, data):
        print(data)
    
    async def on_ping(self, data):
        print(data)
    
    async def wait(self):
        return await self.sio.wait()