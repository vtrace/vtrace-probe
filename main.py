import asyncio
import os
import logging, logging.handlers
import sys
from handler import command_handler
from handler import result_handler
from handler import SocketHandler

# get environment variables

LOG_PATH = os.environ.get("PROBE_LOG_PATH", "/var/log/vtrace-probe.log")
WEBSOCKET_SERVER = os.getenv("WEBSOCKET_SERVER", "ws://localhost:8888")

# setup logging

log = logging.getLogger('vtrace-probe')
log.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
watched_handler = logging.handlers.WatchedFileHandler(LOG_PATH)
console_handler = logging.StreamHandler()

watched_handler.setLevel(logging.DEBUG)
watched_handler.setFormatter(formatter)
console_handler.setLevel(logging.DEBUG)
console_handler.setFormatter(formatter)

log.addHandler(watched_handler)
log.addHandler(console_handler)

# constants

HEARTBEAT_PERIOD = 60

COMMAND_CONSUMER_CNT = 10
RESULT_CONSUMER_CNT = 5

# initializing job queues

command_queue = asyncio.Queue()
result_queue = asyncio.Queue()

# initializing websocket handler

socket_handler = SocketHandler.SocketHandler(WEBSOCKET_SERVER, command_queue)

async def main():
    await socket_handler.connect()
    if not socket_handler.is_connected():
        log.error(f"Couldn't connect to Websocket ${WEBSOCKET_SERVER}")
        sys.exit(1)
    
    asyncio.ensure_future(socket_handler.sio.wait())
    
    # waiting for commands

    command_consumers = [asyncio.get_event_loop()
                        .create_task(command_handler.consume_command(command_queue,
                                                                     result_queue))] * COMMAND_CONSUMER_CNT
    asyncio.ensure_future(asyncio.gather(*command_consumers))
    
    result_consumers = [asyncio.get_event_loop()
                        .create_task(result_handler
                                     .consume_result(result_queue, socket_handler))] * RESULT_CONSUMER_CNT
    
    asyncio.ensure_future(asyncio.gather(*result_consumers))

    await asyncio.sleep(0)

def run():
    try:
        asyncio.ensure_future(main())
        asyncio.get_event_loop().run_forever()
    finally:
        asyncio.get_event_loop().close()

if __name__ == '__main__':
    run()