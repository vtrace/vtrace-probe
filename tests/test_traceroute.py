from context import traceroute
import pytest

@pytest.mark.asyncio
async def test_non_existing_mtr(monkeypatch):
    monkeypatch.setattr(traceroute, 'MTR_BINARY', 'non_existing_binary_abc')
    traceroute_output = await traceroute.run_traceroute('8.8.8.8')
    assert traceroute_output[0] == 127

@pytest.mark.asyncio
async def test_mtr_exists():
    traceroute_output = await traceroute.run_traceroute('8.8.8.8')
    assert traceroute_output[0] != 127 # 127 exit code means not found