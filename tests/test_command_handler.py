import pytest
import asyncio

from unittest.mock import patch, ANY
from asynctest import CoroutineMock

import copy

from context import command_handler
from context import Error

TEST_COMMAND_ID = '9ac2abed-efcb-4507-b1bf-0b0c665accc6'
NON_EXISTING_COMMAND = "1337"

TEST_TRACEROUTE_COMMAND = {
    'type': 'traceroute', 
    'id': TEST_COMMAND_ID, 
    'arguments': {
        'host': '8.8.8.8', 
        'protocol': 'icmp'
    }
}

def failed_command_result(error):
    return {
        'id': TEST_COMMAND_ID,
        'result': {
            "success": False,
            'error': error
        }
    }

def get_consumer_task(command_queue, result_queue):
    return asyncio.get_event_loop().create_task(command_handler.consume_command(command_queue,
                                                                                result_queue))
@pytest.mark.asyncio
async def test_traceroute_command_consumption():
    command_queue = asyncio.Queue()
    result_queue = asyncio.Queue()
    with patch('context.command_handler.traceroute_command', new=CoroutineMock()) as mocked_traceroute:
        consumer_task = get_consumer_task(command_queue, result_queue)
        asyncio.ensure_future(consumer_task)

        await command_queue.put(TEST_TRACEROUTE_COMMAND)
        await asyncio.sleep(1)
        
        consumer_task.cancel()        
        mocked_traceroute.assert_called_once()

@pytest.mark.asyncio
async def test_command_consumption_no_type():
    command_queue = asyncio.Queue()
    result_queue = asyncio.Queue()
    with patch('context.command_handler.traceroute_command', new=CoroutineMock()) as mocked_traceroute:
        consumer_task = get_consumer_task(command_queue, result_queue)
        asyncio.ensure_future(consumer_task)

        invalid_traceroute_command = copy.deepcopy(TEST_TRACEROUTE_COMMAND)
        del invalid_traceroute_command['type']

        await command_queue.put(invalid_traceroute_command)
        await asyncio.sleep(3)

        consumer_task.cancel()

        assert not result_queue.empty()
        result_item = result_queue.get_nowait()

        assert result_item == failed_command_result(Error.COMMAND_TYPE_NOT_DEFINED)

@pytest.mark.asyncio
async def test_command_consumption_no_valid_type():
    command_queue = asyncio.Queue()
    result_queue = asyncio.Queue()
    with patch('context.command_handler.traceroute_command', new=CoroutineMock()) as mocked_traceroute:
        consumer_task = get_consumer_task(command_queue, result_queue)
        asyncio.ensure_future(consumer_task)

        invalid_traceroute_command = copy.deepcopy(TEST_TRACEROUTE_COMMAND)
        invalid_traceroute_command['type'] = NON_EXISTING_COMMAND

        await command_queue.put(invalid_traceroute_command)
        await asyncio.sleep(3)

        consumer_task.cancel()

        assert not result_queue.empty()
        result_item = result_queue.get_nowait()

        assert result_item == failed_command_result(Error.COMMAND_TYPE_DOES_NOT_EXIST)

@pytest.mark.asyncio
async def test_command_consumption_no_arguments():
    command_queue = asyncio.Queue()
    result_queue = asyncio.Queue()
    with patch('context.command_handler.traceroute_command', new=CoroutineMock()) as mocked_traceroute:
        consumer_task = get_consumer_task(command_queue, result_queue)
        asyncio.ensure_future(consumer_task)

        invalid_traceroute_command = copy.deepcopy(TEST_TRACEROUTE_COMMAND)
        del invalid_traceroute_command['arguments']

        await command_queue.put(invalid_traceroute_command)
        await asyncio.sleep(3)

        consumer_task.cancel()

        assert not result_queue.empty()
        result_item = result_queue.get_nowait()

        assert result_item == failed_command_result(Error.COMMAND_ARGUMENTS_NOT_DEFINED)